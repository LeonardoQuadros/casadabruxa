# -*- encoding : utf-8 -*-
namespace :contatos do

  desc "Reenviar todos os e-mails do banco novamente via Delayed Job."
  task reenviar_todos: :environment do

    contatos = Contato.all
    unless contatos.blank?
      puts "Reenviar #{contatos.count} e-mails."
      contatos.each do |contato|
        Mailer.delay.contato contato
      end
    else
      puts "Contatos não localizados."
    end
  end

  desc "Reenviar os e-mails pendentes via Delayed Job."
  task reenviar_pendentes: :environment do

    contatos = Contato.where(enviado: false)
    unless contatos.blank?
      puts "Reenviar #{contatos.count} e-mails."
      contatos.each do |contato|
        Mailer.delay.contato contato
      end
    else
      puts "Contatos não localizados."
    end
  end

end