# -*- encoding : utf-8 -*-
# Set the host name for URL creation
SitemapGenerator::Sitemap.default_host = "http://localhost:3000"

SitemapGenerator::Sitemap.create do
  # Put links creation logic here.
  #
  # The root path '/' and sitemap index file are added automatically for you.
  # Links are added to the Sitemap in the order they are specified.
  #
  # Usage: add(path, options={})
  #        (default options are used if you don't specify)
  #
  # Defaults: :priority => 0.5, :changefreq => 'weekly',
  #           :lastmod => Time.now, :host => default_host
  #
  # Examples:
  #
  # Add '/articles'
  #
  #   add articles_path, :priority => 0.7, :changefreq => 'daily'
  #
  # Add all articles:
  #
  #   Article.find_each do |article|
  #     add article_path(article), :lastmod => article.updated_at
  #   end

  add root_path, priority: 1
  add sobre_path
  add contato_path
  add duvidas_path

  Categoria.ativos.ordenados.find_each do |categoria|
    add categoria_path(categoria), lastmod: categoria.updated_at, changefreq: 'weekly', priority: 0.7 unless categoria.produtos.count.zero?
  end

  Produto.ativos.ordenados.find_each do |produto|
    add produto_path(produto), lastmod: produto.updated_at, changefreq: 'weekly', priority: 0.8
  end

end
