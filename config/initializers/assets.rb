# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'
# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )

# JavaScripts
Rails.application.config.assets.precompile += %w( fileupload.js )
# CSSs.
Rails.application.config.assets.precompile += %w( frontend.css )
Rails.application.config.assets.precompile += %w( fileupload.css )
Rails.application.config.assets.precompile += %w( loading.gif )
Rails.application.config.assets.precompile += %w( media_print.css )
Rails.application.config.assets.precompile += %w( progressbar.gif )
