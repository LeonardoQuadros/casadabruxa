# -*- encoding : utf-8 -*-

# Configuração de formatos padrão de Date e Time
Date::DATE_FORMATS[:default]="%d/%m/%Y"
Time::DATE_FORMATS[:default]="%d/%m/%Y %H:%M"

if Settings.table_exists?

  # Configurações Gerais
  Settings.defaults['CONFIGURACOES.title']                      = "A CASA DA BRUXA"
  Settings.defaults['CONFIGURACOES.logo']                       = "/logo.png"
  Settings.defaults['CONFIGURACOES.logo_after']                 = "/logo.png"



  # Configurações Contato
  Settings.defaults['CONTATO.telefone']           = '1633062000'
  Settings.defaults['CONTATO.email']              = 'franquiacasadabruxa@saffi.com.br'
  Settings.defaults['CONTATO.mensagem']           = 'Obrigado por entrar em contato. Retornaremos o mais breve possível.'
  Settings.defaults['CONTATO.endereco']           = ' Rua Dom Pedro II, 1231 – Sala 04 - São Carlos SP Telefone: 016 3509-4902.'
  Settings.defaults['CONTATO.atendimento_online'] = ''
  Settings.defaults['CONTATO.mapa_iframe']        = ''
  Settings.defaults['CONTATO.meta_descricao']     = ''
  Settings.defaults['CONTATO.meta_script']        = ''
  Settings.defaults['CONTATO.submit_meta_script'] = ''

  # Cadastro de Redes Sociais
  Settings.defaults['REDE_SOCIAIS.site']  = "http://www.acasadabruxa.com.br/"
  Settings.defaults['REDE_SOCIAIS.facebook']  = ""



end