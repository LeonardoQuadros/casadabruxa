# -*- encoding : utf-8 -*-
class Mailer < ActionMailer::Base
  default from: "cadastros@grupofusione.com.br"

  # Enviar link para reset de senha, informações de record e token são geradas pelo device
  def reset_password_instructions(record, token, opts={})
    @token = token
    @resource = record

    mail( to: @resource.email, subject: "Instruções para recuperação de senha") do |format|
      format.html
    end
  end

  # Enviar e-mail de contato
  def contato (contato, options = {})
    @options = options
    @contato = contato
     mail( to: Settings['CONTATO.email'], subject: "#{Settings['CONFIGURACOES.title']} - Solicitação de Contato - #{@contato.nome}" ) do |format|
      format.html
    end
  end

end
