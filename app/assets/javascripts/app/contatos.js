$(document).ready(function() {

  $("div[class$='_flg_casado'] .radio_buttons").click(function() {
    if($(this).prop('value') == '0')
      show_contato_nao_casado();
    if($(this).prop('value') == '1')
      show_contato_casado();
  });


});

function show_contato_nao_casado(){
  $('.contato_casado').hide();
  $('.contato_nao_casado').show();
};

function show_contato_casado(){
  $('.contato_nao_casado').hide();
  $('.contato_casado').show();
};

//=====================================================================================
//=====================================================================================
//=====================================================================================

$(document).ready(function() {

  $("div[class$='_pessoa_fisica'] .radio_buttons").click(function() {
    if($(this).prop('value') == '0')
      show_flg_pessoa_juridica();
    if($(this).prop('value') == '1')
      show_flg_pessoa_fisica();
  });


});

function show_flg_pessoa_fisica(){
  $('.flg_pessoa_juridica').hide();
  $('.flg_pessoa_fisica').show();
};

function show_flg_pessoa_juridica(){
  $('.flg_pessoa_fisica').hide();
  $('.flg_pessoa_juridica').show();
};


//=====================================================================================
//=====================================================================================
//=====================================================================================

$(document).ready(function() {

  $("div[class$='_corretor'] .radio_buttons").click(function() {
    if($(this).prop('value') == '0')
      show_flg_cliente();
    if($(this).prop('value') == '1')
      show_flg_corretor();
  });


});

function show_flg_corretor(){
  $('.flg_cliente').hide();
  $('.flg_corretor').show();
};

function show_flg_cliente(){
  $('.flg_corretor').hide();
  $('.flg_cliente').show();
};


//=====================================================================================
//=====================================================================================
//=====================================================================================

$(document).ready(function() {

  $("div[class$='_flg_avalista'] .radio_buttons").click(function() {
    if($(this).prop('value') == '0')
      show_contato_nao_avalista();
    if($(this).prop('value') == '1')
      show_contato_avalista();
  });


});

function show_contato_nao_avalista(){
  $('.contato_avalista').hide();
  $('.contato_nao_avalista').show();
};

function show_contato_avalista(){
  $('.contato_nao_avalista').hide();
  $('.contato_avalista').show();
};

//=====================================================================================
//=====================================================================================
//=====================================================================================


$(document).ready(function() {

  $("div[class$='_flg_empreendimento'] .radio_buttons").click(function() {
    if($(this).prop('value') == '0')
      show_flg_corretor();
    if($(this).prop('value') == '1')
      show_flg_empreendimento();
  });


});

function show_flg_empreendimento(){
  $('.flg_corretor').hide();
  $('.flg_empreendimento').show();
};

function show_flg_corretor(){
  $('.flg_empreendimento').hide();
  $('.flg_corretor').show();
};



//=====================================================================================
//=====================================================================================
//=====================================================================================


$(document).ready(function() {

  $("div[class$='_flg_salario'] .radio_buttons").click(function() {
    if($(this).prop('value') == '0')
      show_flg_produto();
    if($(this).prop('value') == '1')
      show_flg_salario();
  });


});

function show_flg_salario(){
  $('.flg_produto').hide();
  $('.flg_salario').show();
};

function show_flg_produto(){
  $('.flg_salario').hide();
  $('.flg_produto').show();
};



//=====================================================================================
//=====================================================================================
//=====================================================================================


$(document).ready(function() {

  $("div[class$='_despesa'] .radio_buttons").click(function() {
    if($(this).prop('value') == 'true')
      show_despesa();
    if($(this).prop('value') == 'false')
      show_comissao();
  });


});

function show_despesa(){
  $('.show_comissao').hide();
  $('.show_despesa').show();
};

function show_comissao(){
  $('.show_despesa').hide();
  $('.show_comissao').show();
};


//=====================================================================================
//=====================================================================================
//=====================================================================================