$(function() {

  $(document).on("blur", "input.endereco_cep_ajax", function() {
    if( $(this).val() != "" ) {
    var me = $(this);
      $.ajax({
        url: "/a/enderecos/atualizar_dados_cep",
        method: "POST",
        dataType: "script",
        data: {
          'cep': me.val(),
          'endereco_tipo': $("#endereco_tipo").val()
        },
      });
    }
  });

  $(document).on("blur", "input.endereco_cep_ajax_public", function() {
    if( $(this).val() != "" ) {
    var me = $(this);
      $.ajax({
        url: "/enderecos/atualizar_dados_cep",
        method: "POST",
        dataType: "script",
        data: {
          'cep': me.val(),
          'endereco_tipo': $("#endereco_tipo").val()
        },
      });
    }
  });



});



function atualiza_subbanner(cidade, finalidade){
    $.ajax({
      url: "/enderecos/atualizar_dados",
      method: "GET",
      dataType: "script",
      data: {
        'cidade': cidade,
        'finalidade': finalidade
      },
    });
};