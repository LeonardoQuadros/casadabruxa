// pt-BR
$(function() {
  $(document).ajaxError(function(event, xhr, exception) {
    if (xhr.status == 0) {
      alert("Você não está conectado a internet.\n Verifique sua rede.");
    } else if (xhr.status == 401) {
      alert("Sua sessão expirou. \nVocê vai ser redirecionado para o login.");
      window.location.reload();
    } else if (xhr.status == 404) {
      alert("Ocorreu um erro com o sistema. \nTente novamente mais tarde.");
    } else if (xhr.status == 500) {
      alert("Ocorreu um erro com o sistema. \nTente novamente mais tarde.");
    } else if (exception == "parsererror") {
      alert("Ocorreu um erro com o sistema. \nTente novamente mais tarde.");
    } else if (exception == "timeout") {
      alert("O tempo de espera foi esgotado. \nO sistema provavelmente está sobrecarregado. Tente novamente em alguns instantes.");
    } else {
      // alert("Ocorreu um erro com o sistema. \nTente novamente mais tarde.");
    }
  })
});
