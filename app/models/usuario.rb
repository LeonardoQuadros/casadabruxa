# -*- encoding : utf-8 -*-
class Usuario < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :async, :registerable, :recoverable, :rememberable, :trackable, :validatable, :session_limitable


  # Atributo utilizado para verificar se a senha atual informada está correta
  attr_accessor :password_atual

  validates :email, :nome, presence: true
  validates :email, uniqueness: true


  scope :ordenados, -> {order('usuarios.nome ASC')}
  scope :ativos, -> {where('usuarios.ativo = ?', true)}

  def to_param
    "#{id} #{nome}".parameterize
  end

  # Bloqueia usuários inativos
  def active_for_authentication?
    super and self.ativo
  end

  # Troca o parametro ativo
  def alterar!
    self.update(ativo: !self.ativo)
  end

  def ativo?
    return self.ativo
  end

  def super_admin?
    return self.super_admin
  end

  def self.to_select
    Usuario.ativos.ordenados.collect { |u| [u.nome, u.id] }
  end

  def nome_completo
    "#{self.id} - #{self.nome}"
  end

  def ultimo_login
    return "Nunca logou" if last_sign_in_at.blank?
    retorno = ""
    retorno << "#{I18n.l last_sign_in_at, format: :short}"
    retorno << " - "
    retorno << "#{last_sign_in_ip}"
  end

end