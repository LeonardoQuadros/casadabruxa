# -*- encoding : utf-8 -*-
class Ability
  include CanCan::Ability

  attr_accessor :usuario

  def initialize(usuario)

    usuario ||= Usuario.new
    self.usuario = usuario

    can :manage, :all
    


  end



end
