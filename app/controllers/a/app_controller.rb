# -*- encoding : utf-8 -*-
class A::AppController < ApplicationController

  before_action :authenticate_a_usuario!
  before_filter :guardar_per_page

  check_authorization
  load_and_authorize_resource

  rescue_from CanCan::AccessDenied do |exception|
    redirect_to root_url, alert: I18n.t('login.access_denied')
  end

  # Define o usuario a ser tertado no ability.rb
  def current_ability
    @current_ability ||= Ability.new(current_a_usuario)
  end

  private
    #
    # Uma vez que se altere o per_page, ele vai pra sessão.
    # Isso facilita buscas globais e não perde o per_page em listas com filtros.
    def guardar_per_page
      if params[:per_page].blank?
        params[:per_page] = session[:per_page]
      else
        session[:per_page] = params[:per_page]
      end
    end

end
