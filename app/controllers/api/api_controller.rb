# -*- encoding : utf-8 -*-
module Api
  class ApiController < ActionController::Base

    protect_from_forgery
    skip_before_action :verify_authenticity_token

    before_filter :restrict_access

    private

      def restrict_access
        unless Rails.env.development?
          authenticate_or_request_with_http_token do |token, options|
            ApiKey.exists?(access_token: token)
          end
        end
      end

  end
end

# HTTP Token: Access denied.

# Categorias
# curl  -X GET \
#         -H "Content-Type: application/json" \
#         -H 'Authorization: Token token=fa016a5b8585283e10a0eca3cb244be6' \
#         http://localhost:3000/api/categorias


# Criar uma Categoria
# curl  -X POST \
#         -H "Content-Type: application/json" \
#         -H 'Authorization: Token token=fa016a5b8585283e10a0eca3cb244be6' \
#         -d '{"titulo":"Categoria Teste"}' \
#         http://localhost:3000/api/categorias
