# -*- encoding : utf-8 -*-
# Cria um administrador
if Usuario.where(email: 'contato@casadabruxa.com.br').blank?
  Usuario.create(
    nome: 'Administrador',
    email: 'contato@acasadabruxa.com.br',
    telefone: '(16) 0000-0000',
    password: 'SenhaTeste',
    password_confirmation: 'SenhaTeste',
    super_admin: true
  )
end