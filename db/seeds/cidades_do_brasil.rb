# -*- encoding : utf-8 -*-

connection = ActiveRecord::Base.connection
connection.tables.each do |table|
  connection.execute("TRUNCATE #{table}") unless table == "schema_migrations"
end

sql = File.read('db/seeds/cidades_do_brasil.sql')
statements = sql.split(/;$/)
statements.pop

ActiveRecord::Base.transaction do
  statements.each do |statement|
    connection.execute(statement)
  end
end
