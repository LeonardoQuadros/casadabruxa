# -*- encoding : utf-8 -*-
class AddUniqueSessionIdToUsuarios < ActiveRecord::Migration
  def change
    add_column :usuarios, :unique_session_id, :string, limit: 20
  end
end
