# -*- encoding : utf-8 -*-
class CreateCidades < ActiveRecord::Migration
  def change
    create_table :cidades do |t|
      t.string :nome
      t.references :estado, index: true
    end
  end
end
