# -*- encoding : utf-8 -*-
class CreateEstados < ActiveRecord::Migration
  def change
    create_table :estados do |t|
      t.string :nome
      t.string :acronimo
      t.references :pais
    end
  end
end
