class AddFieldsToContatos < ActiveRecord::Migration
  def change
  	add_column :contatos, :capital_disponivel, :string
  	add_column :contatos, :cidade, :string
  	add_column :contatos, :origem, :string
  end
end
